var ctx = document.getElementById('myChart').getContext('2d');
var dataGraph = {}
var ONE_HOUR = 60 * 60 * 1000; 
const dataLocal = localStorage.getItem('data-graph');

//check if there is an element older than one hour and remove it
const checkElementsLocal = (arrayElements) => {
  const newDate = new Date();
  const arrayFiltered = arrayElements.filter(el => ((newDate.getTime() - (new Date(el.timeUp).getTime())) < ONE_HOUR));
  return arrayFiltered 
}

// transform the data to the graph format and update the graph
const transformDataToGraph = (data) => {
  const labels = []
  const EUR = []
  const GBP = []
  const USD = []
  data.forEach(element => {
    labels.push(element.timeUp);
    EUR.push(element.EUR.rate_float)
    GBP.push(element.GBP.rate_float)
    USD.push(element.USD.rate_float)
  });
  myChart.data.labels = labels;
  myChart.data.datasets[0].data = EUR;
  myChart.data.datasets[1].data = GBP;
  myChart.data.datasets[2].data = USD;
  myChart.update()
}

//call the service for the data
const callService = () => {
  
  fetch('https://api.coindesk.com/v1/bpi/currentprice.json')
  .then(response => response.json())
  .then(data => {
    const {time, bpi:{EUR, GBP, USD}} = data
      const timeUp = time.updated
      //get the actual data
      const actualLocal = localStorage.getItem('data-graph');
      if(actualLocal){
        //verification of the data
        const arrayData = checkElementsLocal(JSON.parse(actualLocal));
        const data = {
          timeUp, EUR, GBP, USD
        }
        //transform and update the graph and the data
        if(timeUp !== arrayData[arrayData.length - 1].timeUp){
          arrayData.push(data);
        } 
        transformDataToGraph(arrayData);
        localStorage.setItem('data-graph', JSON.stringify(arrayData))
        //if there is no data
      } else {
        const data = {
          timeUp, EUR, GBP, USD
        }
        transformDataToGraph([data]);
        localStorage.setItem('data-graph', JSON.stringify([data]))
      }
    }
    );
  }
  
//call the service when the app is loaded
callService()
  
// create the interval
setInterval(() => {
  callService()
}, 30000);


var myChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: dataGraph.labels,
    datasets: [{ 
        data: dataGraph.EUR,
        label: "EUR",
        borderColor: "#3e95cd",
        backgroundColor: "#7bb6dd",
        fill: false,
      }, { 
        data: dataGraph.GBP,
        label: "GBP",
        borderColor: "#3cba9f",
        backgroundColor: "#71d1bd",
        fill: false,
      }, { 
        data: dataGraph.USD,
        label: "USD",
        borderColor: "#ffa500",
        backgroundColor:"#ffc04d",
        fill: false,
      }
    ]
  },
});